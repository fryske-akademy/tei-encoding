# linguistic encoding in TEI #

This project proposes a way to linguistically annotate words using TEI sticking as close to TEI as possible and close to XML techniques. The solution aims to be editor friendly, i.a. through xsd emumeration and documentation, to facilitate easy querying when performing linguistic research and to offer good support for tool development. Widely recognized linguistic terms are used as defined in http://universaldependencies.org, extended with some additional terms. The meaning of all terms is documented in the solution.

## generated rng, xsd's and schematron

Generated sources for js, sql and more are included under src/main/resources. For graphql setup see [Form enum](https://frisian.eu/languageapidocs/form.doc.html) and [FormHelper](https://bitbucket.org/fryske-akademy/languageapi/src/master/model/src/main/java/org/fryske_akademy/languagemodel/FormHelper.java)

## java library

A java library is published to maven central (org.fryske-akademy:TeiLinguisticsFa) with the following functionality:

- jaxb generated classes (jakarta)
- xml (de)serialization
- xml validation (xsd, rng, schematron)
- java documentation class
- a generic XslTransformer

## Highlights of this project:

* [generated reusables](src/main/resources/facustomization)
* maintain odd, generate code

## NEWS

- support for syntactic relations, use link instead of join for compounds (and other relations)
- tei stylesheets 7.56
- jakarta and jdk 17
- dumped problematic reusables
- http://www.fryske-akademy.org/linguistics/2.2 => http://frisian.eu/tei-ud-linguistics (no more namespace change from now on, change log available in odd)
- http://www.fryske-akademy.org/linguistics/2.1 => http://www.fryske-akademy.org/linguistics/2.2
- tei stylesheets 7.53.0
- fa:person => 1,2,3
- updated libs

### what is it? ###

* A TEI customization (corpora_linguistics.odd) to support adding linguistic information to words in TEI encoded documents
* Introduces linguistic attributes on tei:w and tei:m and syntactic relations on tei:link. Content of the attributes are enumerations based on http://universaldependencies.org
* Introduces @lemma on tei:link to support compounds
* Introduces @lemma on tei:m for compound forms

### How do I start? ###

```xml
        <dependency>
            <groupId>org.fryske-akademy</groupId>
            <artifactId>TeiLinguisticsFa</artifactId>
            <version>8.0  or newer</version>
        </dependency>
```
or download from maven central.

You can now use the jaxb classes as TEI data model in Java.

## Example annotation usage

example word encoding:

```xml
<tei:w pos="noun " lemma="frik">Frik</tei:w>
```


example split word encoding:

```xml
<w next="#_48_18" xml:id="_48_14" pos="verb" lemma="falle" fa:number="sing" fa:tense="past">foel</w>
<w pos="adp" lemma="foar">foar</w>
<w pos="det" lemma="de">de</w>
<w pos="noun" lemma="kening" fa:gender="com" fa:number="sing">kening</w>
<w prev="#_48_14" xml:id="_48_18" pos="adp" lemma="del">del</w>
<linkGrp type="UD-SYN" targFunc="head argument">
<link target="#_48_14 #_48_18" lemma="delfalle" ana="compound:prt"/>
</linkGrp>
<w pos="cconj" lemma="en">en</w>
```


example word consist of more lemma's:
```xml
<w>
    <m lemma="dat" type="root" pos="sconj">datst</m>
    <m lemma="do" pos="pron" fa:person="2" fa:prontype="prs" fa:clitic="yes">o</m>
</w>
```
example of a form belonging to two lemmas where the second lemma isn't represented by any character:
```xml
<w>
    <m lemma="dat" type="root" pos="sconj">datst</m>
    <m lemma="do" pos="pron" fa:person="2" fa:prontype="prs"  fa:prodrop=”yes”/>
</w>
```
* Contact Eduard Drenth at the Fryske Akademy for further details

### Who do I talk to? ###

* Eduard Drenth
* Fryske Akademy
