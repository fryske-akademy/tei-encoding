
package org.fa.tei.jaxb.facustomization.docs;


    // GENERATED FILE!!!: USE FOR EXAMPLE "xsltproc OddToDocJava.xsl ../corpora_linguistics.odd > LinguisticDocs.java"

/**
  * @see org.fa.tei.jaxb.facustomization.M
  */
public class LinguisticDocs {

    private LinguisticDocs() {}
    
    /** @param ling use category.value (class.getSimpleName().enum.name())
      * @see org.fa.tei.jaxb.facustomization.M
      */
    public static String getDocumentation(String ling) {
        switch(ling.toLowerCase()) {

            case "islemma.yes":
                return "";

            case "abbr.yes":
                return "";

            case "poss.yes":
                return "";

            case "reflex.yes":
                return "";

            case "prefix.yes":
                return "";

            case "prontype.prs":
                return "personal pronoun or determiner";

            case "prontype.rcp":
                return "reciprocal pronoun";

            case "prontype.art":
                return "Article is a special case of determiner that bears the feature of definiteness";

            case "prontype.int":
                return "interrogative pronoun, determiner, numeral or adverb";

            case "prontype.rel":
                return "relative pronoun, determiner, numeral or adverb";

            case "prontype.ind":
                return "indefinite pronoun, determiner, numeral or adverb";

            case "prontype.emp":
                return "Emphatic pro-adjectives (determiners) emphasize the nominal they depend on.";

            case "prontype.exc":
                return "exclamative determiner";

            case "prontype.dem":
                return "Demonstrative pronouns are often parallel to interrogatives.";

            case "case.nom":
                return "nominative";

            case "case.acc":
                return "accusative";

            case "case.dat":
                return "dative";

            case "case.gen":
                return "genitive";

            case "case.ins":
                return "instrumental / instructive";

            case "case.par":
                return "partitive";

            case "tense.past":
                return "past tense";

            case "tense.pres":
                return "present tense";

            case "tense.fut":
                return "future tense";

            case "voice.act":
                return "The subject of the verb is the doer of the action (agent).";

            case "voice.pass":
                return "The subject of the verb is affected by the action (patient).";

            case "number.sing":
                return "A singular noun denotes one person, animal or thing.";

            case "number.plur":
                return "A plural noun denotes several persons, animals or things.";

            case "number.ptan":
                return "Plurale tantum, some nouns appear only in the plural form even though they denote one thing.";

            case "number.coll":
                return "Collective or mass or singulare tantum applies to words that use grammatical singular to describe sets of objects.";

            case "person.1":
                return "The first person refers just to the speaker / author and in plural one or more additional persons.";

            case "person.2":
                return "The second person refers to the addressee(s).";

            case "person.3":
                return "The third person refers to one or more persons that are neither speakers nor addressees.";

            case "verbtype.mod":
                return "Verbs that take infinitive of another verb as argument and add various modes of possibility, necessity etc.";

            case "verbtype.tense":
                return "Verb used to create periphrastic verb forms (tenses, passives etc.).";

            case "verbform.inf":
                return "Infinitive is the citation form of verbs in many languages.";

            case "verbform.part":
                return "Participle is a non-finite verb form that shares properties of verbs and adjectives.";

            case "verbform.ger":
                return "Gerund is a non-finite verb form that shares properties of verbs and nouns.";

            case "verbform.conv":
                return "The converb, also called adverbial participle or transgressive, is a non-finite verb form that shares properties of verbs and adverbs.";

            case "polite.infm":
                return "usually meant for communication with family members and close friends.";

            case "polite.form":
                return "usually meant for communication with strangers and people of higher social status.";

            case "numtype.ord":
                return "ordinal number (first, second,..)";

            case "numtype.card":
                return "cardinal number (one, two, many,....)";

            case "degree.pos":
                return "positive, first degree";

            case "degree.cmp":
                return "comparative, second degree";

            case "degree.sup":
                return "superlative, third degree";

            case "degree.dim":
                return "Added to features in universaldependencies. Diminutive.";

            case "mood.imp":
                return "The speaker uses imperative to order or ask the addressee to do the action of the verb.";

            case "mood.sub":
                return "The subjunctive mood is used under certain circumstances in subordinate clauses, typically for actions that are subjective or otherwise uncertain.";

            case "mood.ind":
                return "A verb in indicative merely states that something happens, has happened or will happen.";

            case "gender.masc":
                return "masculine gender";

            case "gender.fem":
                return "feminine gender";

            case "gender.neut":
                return "neuter gender";

            case "gender.com":
                return "Some languages do not distinguish masculine/feminine but they do distinguish neuter vs. non-neuter. The non-neuter is called common gender.";

            case "hyph.yes":
                return "";

            case "prodrop.yes":
                return "";

            case "clitic.yes":
                return "";

            case "compound.yes":
                return "";

            case "inflection.infl":
                return "Not in universaldependencies. inflected";

            case "inflection.uninf":
                return "Not in universaldependencies. uninflected";

            case "suffix.yes":
                return "";

            case "valency.1":
                return "An intransitive verb takes one argument (no object)";

            case "valency.2":
                return "A monotransitive verb takes two arguments (of which one object)";

            case "valency.3":
                return "A ditransitive verb takes three arguments (of which a direct and an indirect object)";

            case "convertedfrom.adj":
                return "Not in universaldependencies. adjective used as another category";

            case "convertedfrom.adv":
                return "Not in universaldependencies. adverb used as another category";

            case "convertedfrom.ver":
                return "Not in universaldependencies. verb used as another category";

            case "convertedfrom.num":
                return "Not in universaldependencies. numeral used as another category";

            case "convertedfrom.pro":
                return "Not in universaldependencies. pronomen used as another category";

            case "convertedfrom.part":
                return "Not in universaldependencies. verbform part used as another category";

            case "predicate.yes":
                return "Not in universaldependencies. statement about the subject";

            case "construction.attr":
                return "Not in universaldependencies. attributive";

            case "pos.adj":
                return "Adjectives are words that typically modify nouns and specify their properties or attributes.";

            case "pos.adp":
                return "Adposition is a cover term for prepositions and postpositions.";

            case "pos.adv":
                return "Adverbs are words that typically modify verbs for such categories as time, place, direction or manner.";

            case "pos.aux":
                return "An auxiliary is a function word that accompanies the lexical verb of a verb phrase and expresses grammatical distinctions not carried by the lexical verb, such as person, number, tense, mood, aspect, voice or evidentiality.";

            case "pos.cconj":
                return "A coordinating conjunction is a word that links words or larger constituents without syntactically subordinating one to the other and expresses a semantic relationship between them.";

            case "pos.det":
                return "Determiners are words that modify nouns or noun phrases and express the reference of the noun phrase in context.";

            case "pos.intj":
                return "An interjection is a word that is used most often as an exclamation or part of an exclamation.";

            case "pos.noun":
                return "Nouns are a part of speech typically denoting a person, place, thing, animal or idea.";

            case "pos.num":
                return "A numeral is a word, functioning most typically as a determiner, adjective or pronoun, that expresses a number and a relation to the number, such as quantity, sequence, frequency or fraction.";

            case "pos.part":
                return "Particles are function words that must be associated with another word or phrase to impart meaning and that do not satisfy definitions of other universal parts of speech.";

            case "pos.pron":
                return "Pronouns are words that substitute for nouns or noun phrases, whose meaning is recoverable from the linguistic or extralinguistic context.";

            case "pos.propn":
                return "A proper noun is a noun (or nominal content word) that is the name (or part of the name) of a specific individual, place, or object.";

            case "pos.punct":
                return "Punctuation marks are non-alphabetical characters and character groups used in many languages to delimit linguistic units in printed text.";

            case "pos.conj":
                return "Not in universaldependencies. A conjunction is a conjunction that links constructions, where no assumption about the role of the constructions is made.";

            case "pos.sconj":
                return "A subordinating conjunction is a conjunction that links constructions by making one of them a constituent of the other.";

            case "pos.sym":
                return "A symbol is a word-like entity that differs from ordinary words by form, function, or both.";

            case "pos.verb":
                return "A verb is a member of the syntactic class of words that typically signal events and actions.";

            case "pos.x":
                return "The tag X is used for words that for some reason cannot be assigned a real part-of-speech category.";

            case "UD-SYN.acl":
                return "";

            case "UD-SYN.acl:relcl":
                return "";

            case "UD-SYN.advcl":
                return "";

            case "UD-SYN.advmod":
                return "";

            case "UD-SYN.amod":
                return "";

            case "UD-SYN.appos":
                return "";

            case "UD-SYN.aux":
                return "";

            case "UD-SYN.aux:pass":
                return "";

            case "UD-SYN.case":
                return "";

            case "UD-SYN.cc":
                return "";

            case "UD-SYN.ccomp":
                return "";

            case "UD-SYN.cc_preconj":
                return "";

            case "UD-SYN.conj":
                return "";

            case "UD-SYN.cop":
                return "";

            case "UD-SYN.csubj":
                return "";

            case "UD-SYN.dep":
                return "";

            case "UD-SYN.det":
                return "";

            case "UD-SYN.discourse":
                return "";

            case "UD-SYN.expl":
                return "";

            case "UD-SYN.expl:pass":
                return "";

            case "UD-SYN.expl:pv":
                return "";

            case "UD-SYN.fixed":
                return "";

            case "UD-SYN.flat":
                return "";

            case "UD-SYN.flat_foreign":
                return "";

            case "UD-SYN.flat_name":
                return "";

            case "UD-SYN.iobj":
                return "";

            case "UD-SYN.mark":
                return "";

            case "UD-SYN.nmod":
                return "";

            case "UD-SYN.nmod:poss":
                return "";

            case "UD-SYN.nsubj":
                return "";

            case "UD-SYN.nsubj:pass":
                return "";

            case "UD-SYN.nummod":
                return "";

            case "UD-SYN.obj":
                return "";

            case "UD-SYN.obl":
                return "";

            case "UD-SYN.parataxis":
                return "";

            case "UD-SYN.punct":
                return "";

            case "UD-SYN.root":
                return "";

            case "UD-SYN.xcomp":
                return "";

            case "UD-SYN.compound:prt":
                return "";

            default:
                return "undocumented";
        }
    }
    
    /** @param ling use category (class.getSimpleName())
      * @see org.fa.tei.jaxb.facustomization.M
      */
    public static String getCategoryDocumentation(String ling) {
        switch(ling.toLowerCase()) {

            case "islemma":
                return "Boolean, is this a base form";

            case "abbr":
                return "Boolean feature. Is this an abbreviation?";

            case "poss":
                return "Boolean feature. Is this word possessive?";

            case "reflex":
                return "Boolean feature, typically of pronouns or determiners. It tells whether the word is reflexive, i.e. refers to the subject of its clause.?";

            case "prefix":
                return "Boolean feature, Is this a prefix word in a compound, that usually cannot stand on its own?";

            case "prontype":
                return "This feature typically applies to pronouns, pronominal adjectives (determiners), pronominal numerals (quantifiers) and pronominal adverbs.";

            case "case":
                return "Case is usually an inflectional feature of nouns.";

            case "tense":
                return "Tense is typically a feature of verbs.";

            case "voice":
                return "Voice is typically a feature of verbs.";

            case "number":
                return "Number is usually an inflectional feature of nouns.";

            case "person":
                return "Person is typically feature of personal and possessive pronouns / determiners, and of verbs.";

            case "verbtype":
                return "distinctions on top of verb and aux.";

            case "verbform":
                return "form of verb or deverbative.";

            case "polite":
                return "Various languages have various means to express politeness or respect.";

            case "numtype":
                return "numeral type.";

            case "degree":
                return "Degree of comparison is typically an inflectional feature of some adjectives and adverbs.";

            case "mood":
                return "Mood is a feature that expresses modality and subclassifies finite verb forms.";

            case "gender":
                return "gender.";

            case "hyph":
                return "Is this part of a hyphenated compound? Depending on tokenization, the compound may be one token or be split to several tokens; then the tokens need tags.";

            case "prodrop":
                return "Added for Frisian to MISC in universaldependencies. pronoun drop, omission of pronouns because they can be inferred";

            case "clitic":
                return "Added for Frisian to features in universaldependencies. Most personal pronouns have a clitic form, which is the result of either vowel deletion, vowel reduction, monophthongization or schwa deletion, while there are also cases of suppletion.";

            case "compound":
                return "Added for Frisian to features in universaldependencies. The univerbation of two or more words.";

            case "inflection":
                return "Not in universaldependencies. The modification of a word to express different grammatical categories such as tense, case, voice, aspect, person.";

            case "suffix":
                return "Not in universaldependencies Boolean feature, Is this a suffix word in a compound, that usually cannot stand on its own?";

            case "valency":
                return "Not in universaldependencies. Verb valency or valence is the number of arguments controlled by a verbal predicate.";

            case "convertedfrom":
                return "Not in universaldependencies. Words belonging to one part of speech category used as another category.";

            case "predicate":
                return "Not in universaldependencies. Predicate.";

            case "construction":
                return "Not in universaldependencies. Construction.";

            case "pos":
                return "These tags mark the core part-of-speech categories.";

            case "UD-SYN":
                return "syntactic relations:";

            case "head argument":
                return "function of first entry in link/@target is head, of second entry is argument";

            default:
                return "undocumented";
        }
    }
}            
    