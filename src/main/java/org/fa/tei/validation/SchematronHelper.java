package org.fa.tei.validation;

/*-
 * #%L
 * TeiLinguisticsFa
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import name.dmaus.schxslt.Result;
import name.dmaus.schxslt.Schematron;
import name.dmaus.schxslt.SchematronException;
import org.xml.sax.InputSource;

import javax.xml.transform.sax.SAXSource;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;

/**
 * Can validate xml against schematron generated from fryske akademy tei dictionaries customization
 */
public class SchematronHelper {

    public static final String CORPORA_LINGUISTICS_SCH = "/facustomization/schematron/corpora_linguistics.sch";


    private static final Schematron SCHEMATRON;
    static {
        try {
            SCHEMATRON = new Schematron(new SAXSource(
                    new InputSource(SchematronHelper.class.getResourceAsStream(CORPORA_LINGUISTICS_SCH))));
        } catch (SchematronException e) {
            throw new IllegalStateException("unable to initialize schematron", e);
        }
    }

    /**
     * validates using schematron, the result holds success or failures
     *
     * @param xml
     * @throws SchematronException when transforming fails, NOT when validation fails
     */
    public static Result validate(URL xml) throws IOException, SchematronException {
        return SCHEMATRON.validate(new SAXSource(new InputSource(xml.openStream())));
    }

    /**
     * validates using schematron, the result holds success or failures
     *
     * @param xml
     * @throws SchematronException when transforming fails, NOT when validation fails
     */
    public static Result validate(String xml) throws SchematronException {
        return SCHEMATRON.validate(new SAXSource(new InputSource(new StringReader(xml))));
    }

    /**
     * validate all files in first file or directory argument (ValidationException indicates validation failure)
     *
     * @param args
     * @throws ValidationException when validation fails
     */
    public static void main(String[] args) throws IOException, SchematronException {
        if (args != null && args.length > 0) {
            File f = new File(args[0]);
            if (f.isFile()) {
                Result r = validate(new URL(args[0]));
                if (!r.isValid()) {
                    System.err.println(f.getPath() + " invalid");
                    throw new ValidationException(r.getValidationMessages().toString());
                }
            } else if (f.isDirectory())
                Files.walk(f.toPath()).filter(p -> p.toFile().isFile()).forEach(
                        p -> {
                            try {
                                Result r = validate(new URL(args[0]));
                                if (!r.isValid()) {
                                    System.err.println(p.toFile().getPath() + " invalid");
                                    throw new ValidationException(r.getValidationMessages().toString());
                                }
                            } catch (IOException|SchematronException e) {
                                throw new IllegalStateException(e);
                            }
                        }
                );
        }
    }

}
