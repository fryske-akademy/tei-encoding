<?xml version="1.0" encoding="utf-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <title>ISO Schematron rules</title>
   <!-- This file generated 2025-02-22T06:13:08Z by 'extract-isosch.xsl'. -->
   <!-- ********************* -->
   <!-- namespaces, declared: -->
   <!-- ********************* -->
   <ns prefix="tei" uri="http://www.tei-c.org/ns/1.0"/>
   <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
   <ns prefix="rng" uri="http://relaxng.org/ns/structure/1.0"/>
   <ns prefix="rna" uri="http://relaxng.org/ns/compatibility/annotations/1.0"/>
   <ns prefix="sch" uri="http://purl.oclc.org/dsdl/schematron"/>
   <ns prefix="sch1x" uri="http://www.ascc.net/xml/schematron"/>
   <!-- ********************* -->
   <!-- namespaces, implicit: -->
   <!-- ********************* -->
   <ns prefix="esp-d2e57765" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e94858" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e94876" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e94894" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e94912" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e94930" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e94949" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95011" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95058" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95089" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95115" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95151" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95183" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95209" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95245" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95271" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95297" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95333" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95365" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95401" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95419" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95434" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95449" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95464" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95488" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95503" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95531" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95575" uri="http://frisian.eu/tei-ud-linguistics"/>
   <ns prefix="esp-d2e95593" uri="http://frisian.eu/tei-ud-linguistics"/>
   <!-- ************ -->
   <!-- constraints: -->
   <!-- ************ -->
   <pattern id="schematron-constraint-corpora_linguistics-att.cmc-generatedBy-CMC_generatedBy_within_post-1">
      <rule context="tei:*[@generatedBy]">
         <assert test="ancestor-or-self::tei:post">The @generatedBy attribute is for use within a &lt;post&gt; element.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.datable.w3c-att-datable-w3c-when-2">
      <rule context="tei:*[@when]">
         <report test="@notBefore|@notAfter|@from|@to" role="nonfatal">The @when attribute cannot be used with any other att.datable.w3c attributes.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.datable.w3c-att-datable-w3c-from-3">
      <rule context="tei:*[@from]">
         <report test="@notBefore" role="nonfatal">The @from and @notBefore attributes cannot be used together.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.datable.w3c-att-datable-w3c-to-4">
      <rule context="tei:*[@to]">
         <report test="@notAfter" role="nonfatal">The @to and @notAfter attributes cannot be used together.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.global.source-source-only_1_ODD_source-5">
      <rule context="tei:*[@source]">
         <let name="srcs" value="tokenize( normalize-space(@source),' ')"/>
         <report test="( self::tei:classRef               | self::tei:dataRef               | self::tei:elementRef               | self::tei:macroRef               | self::tei:moduleRef               | self::tei:schemaSpec )               and               $srcs[2]">
              When used on a schema description element (like
              <value-of select="name(.)"/>), the @source attribute
              should have only 1 value. (This one has <value-of select="count($srcs)"/>.)
            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.measurement-att-measurement-unitRef-6">
      <rule context="tei:*[@unitRef]">
         <report test="@unit" role="info">The @unit attribute may be unnecessary when @unitRef is present.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.typed-subtypeTyped-7">
      <rule context="tei:*[@subtype]">
         <assert test="@type">The <name/> element should not be categorized in detail with @subtype unless also categorized in general with @type</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.pointing-targetLang-targetLang-8">
      <rule context="tei:*[not(self::tei:schemaSpec)][@targetLang]">
         <assert test="@target">@targetLang should only be used on <name/> if @target is specified.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.spanning-spanTo-spanTo-points-to-following-9">
      <rule context="tei:*[@spanTo]">
         <assert test="id(substring(@spanTo,2)) and following::*[@xml:id=substring(current()/@spanTo,2)]">
The element indicated by @spanTo (<value-of select="@spanTo"/>) must follow the current element <name/>
         </assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.styleDef-schemeVersion-schemeVersionRequiresScheme-10">
      <rule context="tei:*[@schemeVersion]">
         <assert test="@scheme and not(@scheme = 'free')">
              @schemeVersion can only be used if @scheme is specified.
            </assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-att.calendarSystem-calendar-calendar_attr_on_empty_element-11">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
              systems or calendars to which the date represented by the content of this element belongs,
              but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-p-abstractModel-structure-p-in-ab-or-p-12">
      <rule context="tei:p">
         <report test="(ancestor::tei:ab or ancestor::tei:p) and                        not( ancestor::tei:floatingText                           | parent::tei:exemplum                           | parent::tei:item                           | parent::tei:note                           | parent::tei:q                           | parent::tei:quote                           | parent::tei:remarks                           | parent::tei:said                           | parent::tei:sp                           | parent::tei:stage                           | parent::tei:cell                           | parent::tei:figure )">
          Abstract model violation: Paragraphs may not occur inside other paragraphs or ab elements.
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-p-abstractModel-structure-p-in-l-or-lg-13">
      <rule context="tei:p">
         <report test="( ancestor::tei:l  or  ancestor::tei:lg ) and                        not( ancestor::tei:floatingText                           | parent::tei:figure                           | parent::tei:note )">
          Abstract model violation: Lines may not contain higher-level structural elements such as div, p, or ab, unless p is a child of figure or note, or is a descendant of floatingText.
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-desc-deprecationInfo-only-in-deprecated-14">
      <rule context="tei:desc[ @type eq 'deprecationInfo']">
         <assert test="../@validUntil">Information about a
        deprecation should only be present in a specification element
        that is being deprecated: that is, only an element that has a
        @validUntil attribute should have a child &lt;desc
        type="deprecationInfo"&gt;.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-rt-target-rt-target-not-span-15">
      <rule context="tei:rt/@target">
         <report test="../@from | ../@to">When target= is present, neither from= nor to= should be.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-rt-from-rt-from-16">
      <rule context="tei:rt/@from">
         <assert test="../@to">When from= is present, the to= attribute of <name/> is required.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-rt-to-rt-to-17">
      <rule context="tei:rt/@to">
         <assert test="../@from">When to= is present, the from= attribute of <name/> is required.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-name-calendar-calendar-check-name-18">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-ptr-ptrAtts-19">
      <rule context="tei:ptr">
         <report test="@target and @cRef">Only one of the attributes @target and @cRef may be supplied on <name/>.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-ref-refAtts-20">
      <rule context="tei:ref">
         <report test="@target and @cRef">Only one of the attributes @target' and @cRef' may be supplied on <name/>
         </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-list-gloss-list-must-have-labels-21">
      <rule context="tei:list[@type='gloss']">
         <assert test="tei:label">The content of a "gloss" list should include a sequence of one or more pairs of a label element followed by an item element</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-author-calendar-calendar-check-author-22">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-editor-calendar-calendar-check-editor-23">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-resp-calendar-calendar-check-resp-24">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-title-calendar-calendar-check-title-25">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-meeting-calendar-calendar-check-meeting-26">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-relatedItem-targetorcontent1-27">
      <rule context="tei:relatedItem">
         <report test="@target and count( child::* ) &gt; 0">If the @target attribute on <name/> is used, the relatedItem element must be empty</report>
         <assert test="@target or child::*">A relatedItem element should have either a @target attribute or a child element to indicate the related bibliographic item</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-l-abstractModel-structure-l-in-l-28">
      <rule context="tei:l">
         <report test="ancestor::tei:l[not(.//tei:note//tei:l[. = current()])]">Abstract model violation: Lines may not contain lines or lg elements.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-lg-atleast1oflggapl-29">
      <rule context="tei:lg">
         <assert test="count(descendant::tei:lg|descendant::tei:l|descendant::tei:gap) &gt; 0">An lg element must contain at least one child l, lg, or gap element.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-lg-abstractModel-structure-lg-in-l-30">
      <rule context="tei:lg">
         <report test="ancestor::tei:l[not(.//tei:note//tei:lg[. = current()])]">Abstract model violation: Lines may not contain line groups.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-s-noNestedS-31">
      <rule context="tei:s">
         <report test="tei:s">You may not nest one s element within another: use seg instead</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-span-target-from-32">
      <rule context="tei:span">
         <report test="@from and @target">
          Only one of the attributes @target and @from may be supplied on <name/>
         </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-span-targetto-33">
      <rule context="tei:span">
         <report test="@to and @target">
          Only one of the attributes @target and @to may be supplied on <name/>
         </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-span-tonotfrom-34">
      <rule context="tei:span">
         <report test="@to and not(@from)">
          If @to is supplied on <name/>, @from must be supplied as well
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-span-tofrom-35">
      <rule context="tei:span">
         <report test="contains(normalize-space(@to),' ') or contains(normalize-space(@from),' ')">
          The attributes @to and @from on <name/> may each contain only a single value
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-link-lemma_check-36">
      <rule context="tei:link">
         <report test="@lemma and not(@ana='compound:prt')">
            <value-of select="@ana"/> cannot have @lemma, only compound:prt
                            </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-link-linkTargets3-37">
      <rule context="tei:link">
         <assert test="contains(normalize-space(@target),' ')">You must supply at least two values for @target or  on <name/>
         </assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-ab-abstractModel-structure-ab-in-l-or-lg-38">
      <rule context="tei:ab">
         <report test="(ancestor::tei:l or ancestor::tei:lg) and not( ancestor::tei:floatingText |parent::tei:figure |parent::tei:note )">
          Abstract model violation: Lines may not contain higher-level divisions such as p or ab, unless ab is a child of figure or note, or is a descendant of floatingText.
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-join-joinTargets3-39">
      <rule context="tei:join">
         <assert test="contains( normalize-space( @target ),' ')">
          You must supply at least two values for @target on <name/>
         </assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-standOff-nested_standOff_should_be_typed-40">
      <rule context="tei:standOff">
         <assert test="@type or not(ancestor::tei:standOff)">This
        <name/> element must have a @type attribute, since it is
        nested inside a <name/>
         </assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-sponsor-calendar-calendar-check-sponsor-41">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-funder-calendar-calendar-check-funder-42">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-principal-calendar-calendar-check-principal-43">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-idno-calendar-calendar-check-idno-44">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-licence-calendar-calendar-check-licence-45">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-quotation-quotationContents-46">
      <rule context="tei:quotation">
         <report test="not( @marks )  and  not( tei:p )">
          On <name/>, either the @marks attribute should be used, or a paragraph of description provided
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-citeStructure-match-citestructure-outer-match-47">
      <rule context="tei:citeStructure[not(parent::tei:citeStructure)]">
         <assert test="starts-with(@match,'/')">An XPath in @match on the outer <name/> must start with '/'.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-citeStructure-match-citestructure-inner-match-48">
      <rule context="tei:citeStructure[parent::tei:citeStructure]">
         <assert test="not(starts-with(@match,'/'))">An XPath in @match must not start with '/' except on the outer <name/>.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-unitDecl-calendar-calendar-check-unitDecl-49">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-unitDef-calendar-calendar-check-unitDef-50">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-conversion-calendar-calendar-check-conversion-51">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-application-calendar-calendar-check-application-52">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-creation-calendar-calendar-check-creation-53">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-change-calendar-calendar-check-change-54">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-div-abstractModel-structure-div-in-l-or-lg-58">
      <rule context="tei:div">
         <report test="(ancestor::tei:l or ancestor::tei:lg) and not(ancestor::tei:floatingText)">
          Abstract model violation: Lines may not contain higher-level structural elements such as div, unless div is a descendant of floatingText.
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-div-abstractModel-structure-div-in-ab-or-p-59">
      <rule context="tei:div">
         <report test="(ancestor::tei:p or ancestor::tei:ab) and not(ancestor::tei:floatingText)">
          Abstract model violation: p and ab may not contain higher-level structural elements such as div, unless div is a descendant of floatingText.
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-facsimile-no_facsimile_text_nodes-60">
      <rule context="tei:facsimile//tei:line | tei:facsimile//tei:zone">
         <report test="child::text()[ normalize-space(.) ne '']">
          A facsimile element represents a text with images, thus
          transcribed text should not be present within it.
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-path-pathmustnotbeclosed-61">
      <rule context="tei:path[@points]">
         <let name="firstPair" value="tokenize( normalize-space( @points ), ' ')[1]"/>
         <let name="lastPair"
              value="tokenize( normalize-space( @points ), ' ')[last()]"/>
         <let name="firstX" value="xs:float( substring-before( $firstPair, ',') )"/>
         <let name="firstY" value="xs:float( substring-after( $firstPair, ',') )"/>
         <let name="lastX" value="xs:float( substring-before( $lastPair, ',') )"/>
         <let name="lastY" value="xs:float( substring-after( $lastPair, ',') )"/>
         <report test="$firstX eq $lastX and $firstY eq $lastY">The first and
          last elements of this path are the same. To specify a closed polygon, use
          the zone element rather than the path element. </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-addSpan-addSpan-requires-spanTo-62">
      <rule context="tei:addSpan">
         <assert test="@spanTo">The @spanTo attribute of <name/> is required.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-damageSpan-damageSpan-requires-spanTo-64">
      <rule context="tei:damageSpan">
         <assert test="@spanTo">The @spanTo attribute of <name/> is required.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-delSpan-delSpan-requires-spanTo-66">
      <rule context="tei:delSpan">
         <assert test="@spanTo">The @spanTo attribute of <name/> is required.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-subst-substContents1-68">
      <rule context="tei:subst">
         <assert test="child::tei:add and (child::tei:del or child::tei:surplus)">
            <name/> must have at least one child add and at least one child del or surplus</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-orgName-calendar-calendar-check-orgName-69">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-persName-calendar-calendar-check-persName-70">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-placeName-calendar-calendar-check-placeName-71">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-bloc-calendar-calendar-check-bloc-72">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-country-calendar-calendar-check-country-73">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-region-calendar-calendar-check-region-74">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-settlement-calendar-calendar-check-settlement-75">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-district-calendar-calendar-check-district-76">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-offset-calendar-calendar-check-offset-77">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-geogName-calendar-calendar-check-geogName-78">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-geogFeat-calendar-calendar-check-geogFeat-79">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-affiliation-calendar-calendar-check-affiliation-80">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-age-calendar-calendar-check-age-81">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-birth-calendar-calendar-check-birth-82">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-climate-calendar-calendar-check-climate-83">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-death-calendar-calendar-check-death-84">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-education-calendar-calendar-check-education-85">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-event-calendar-calendar-check-event-86">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
            systems or calendars to which the date represented by the content of this element belongs,
            but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-faith-calendar-calendar-check-faith-87">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-floruit-calendar-calendar-check-floruit-88">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-gender-calendar-calendar-check-gender-89">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-langKnowledge-calendar-calendar-check-langKnowledge-90">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-langKnown-calendar-calendar-check-langKnown-91">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-location-calendar-calendar-check-location-92">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-nationality-calendar-calendar-check-nationality-93">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-occupation-calendar-calendar-check-occupation-94">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-persPronouns-calendar-calendar-check-persPronouns-95">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-population-calendar-calendar-check-population-96">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-relation-ref-or-key-or-name-97">
      <rule context="tei:relation">
         <assert test="@ref or @key or @name">One of the attributes @name, @ref or @key must be supplied</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-relation-active-mutual-98">
      <rule context="tei:relation">
         <report test="@active and @mutual">Only one of the attributes @active and @mutual may be supplied</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-relation-active-passive-99">
      <rule context="tei:relation">
         <report test="@passive and not(@active)">the attribute @passive may be supplied only if the attribute @active is supplied</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-relation-calendar-calendar-check-relation-100">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
              systems or calendars to which the date represented by the content of this element belongs,
              but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-residence-calendar-calendar-check-residence-101">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-sex-calendar-calendar-check-sex-102">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-socecStatus-calendar-calendar-check-socecStatus-103">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-state-calendar-calendar-check-state-104">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-terrain-calendar-calendar-check-terrain-105">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-trait-calendar-calendar-check-trait-106">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-objectName-calendar-calendar-check-objectName-107">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-rdgGrp-only1lem-108">
      <rule context="tei:rdgGrp">
         <assert test="count(tei:lem) lt 2">Only one &lt;lem&gt; element may appear within a &lt;rdgGrp&gt;</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-variantEncoding-location-variantEncodingLocation-109">
      <rule context="tei:variantEncoding">
         <report test="@location eq 'external' and @method eq 'parallel-segmentation'">
              The @location value "external" is inconsistent with the
              parallel-segmentation method of apparatus markup.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-msDesc-one_ms_singleton_max-110">
      <rule context="tei:msContents|tei:physDesc|tei:history|tei:additional">
         <let name="gi" value="name(.)"/>
         <report test="preceding-sibling::*[ name(.) eq $gi ]                           and                           not( following-sibling::*[ name(.) eq $gi ] )">
          Only one <name/> is allowed as a child of <value-of select="name(..)"/>.
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-catchwords-catchword_in_msDesc-111">
      <rule context="tei:catchwords">
         <assert test="ancestor::tei:msDesc or ancestor::tei:egXML">The <name/> element should not be used outside of msDesc.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-dimensions-duplicateDim-112">
      <rule context="tei:dimensions">
         <report test="count(tei:width) gt 1">
          The element <name/> may appear once only
        </report>
         <report test="count(tei:height) gt 1">
          The element <name/> may appear once only
        </report>
         <report test="count(tei:depth) gt 1">
          The element <name/> may appear once only
        </report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-origPlace-calendar-calendar-check-origPlace-113">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-secFol-secFol_in_msDesc-114">
      <rule context="tei:secFol">
         <assert test="ancestor::tei:msDesc or ancestor::tei:egXML">The <name/> element should not be used outside of msDesc.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-signatures-signatures_in_msDesc-115">
      <rule context="tei:signatures">
         <assert test="ancestor::tei:msDesc or ancestor::tei:egXML">The <name/> element should not be used outside of msDesc.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-stamp-calendar-calendar-check-stamp-116">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-msIdentifier-msId_minimal-117">
      <rule context="tei:msIdentifier">
         <report test="not( parent::tei:msPart )                           and                           ( child::*[1]/self::idno  or  child::*[1]/self::altIdentifier  or  normalize-space(.) eq '')">An msIdentifier must contain either a repository or location.</report>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-altIdentifier-calendar-calendar-check-altIdentifier-118">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-binding-calendar-calendar-check-binding-119">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-seal-calendar-calendar-check-seal-120">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-origin-calendar-calendar-check-origin-121">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-provenance-calendar-calendar-check-provenance-122">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-acquisition-calendar-calendar-check-acquisition-123">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <pattern id="schematron-constraint-corpora_linguistics-custEvent-calendar-calendar-check-custEvent-124">
      <rule context="tei:*[@calendar]">
         <assert test="string-length( normalize-space(.) ) gt 0"> @calendar indicates one or more
                        systems or calendars to which the date represented by the content of this element belongs,
                        but this <name/> element has no textual content.</assert>
      </rule>
   </pattern>
   <!-- *********** -->
   <!-- deprecated: -->
   <!-- *********** -->
   <pattern>
      <rule context="tei:name">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the name element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:author">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the author element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:editor">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the editor element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:resp">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the resp element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:title">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the title element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:meeting">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the meeting element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:sponsor">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the sponsor element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:funder">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the funder element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:principal">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the principal element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:idno">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the idno element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:licence">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the licence element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:unitDecl">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the unitDecl element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:unitDef">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the unitDef element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:conversion">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the conversion element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:application">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the application element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:creation">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the creation element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:change">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the change element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:orgName">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the orgName element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:persName">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the persName element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:placeName">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the placeName element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:bloc">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the bloc element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:country">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the country element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:region">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the region element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:settlement">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the settlement element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:district">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the district element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:offset">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the offset element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:geogName">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the geogName element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:geogFeat">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the geogFeat element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:affiliation">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the affiliation element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:age">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the age element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:birth">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the birth element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:climate">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the climate element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:death">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the death element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:education">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the education element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:event">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the event element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:faith">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the faith element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:floruit">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the floruit element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:gender">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the gender element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:langKnowledge">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the langKnowledge element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:langKnown">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the langKnown element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:location">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the location element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:nationality">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the nationality element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:occupation">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the occupation element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:persPronouns">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the persPronouns element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:population">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the population element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:relation">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the relation element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:residence">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the residence element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:sex">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the sex element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:socecStatus">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the socecStatus element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:state">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the state element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:terrain">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the terrain element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:trait">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the trait element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:objectName">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the objectName element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:origPlace">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the origPlace element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:stamp">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the stamp element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:altIdentifier">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the altIdentifier element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:binding">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the binding element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:seal">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the seal element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:origin">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the origin element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:provenance">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the provenance element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:acquisition">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the acquisition element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
   <pattern>
      <rule context="tei:custEvent">
         <report test="@calendar" role="nonfatal">WARNING: use of deprecated attribute — @calendar of the custEvent element will be removed from the TEI on 2024-11-11.
                </report>
      </rule>
   </pattern>
</schema>
