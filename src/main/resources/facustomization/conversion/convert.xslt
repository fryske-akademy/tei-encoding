<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fa21="http://www.fryske-akademy.org/linguistics/2.1"
                xmlns:fa="http://frisian.eu/tei-ud-linguistics"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="fa21"
                version="1.0">
<!--TODO saxonica 11.3 generates redundant namespace declarations!!-->
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="tei:TEI">
        <tei:TEI xmlns:fa="http://frisian.eu/tei-ud-linguistics" fa:linguisticsversion="1">
            <xsl:apply-templates/>
        </tei:TEI>
    </xsl:template>

    <xsl:template match="@fa21:person[.='first']">
        <xsl:attribute name="fa:person">1</xsl:attribute>
    </xsl:template>
    <xsl:template match="@fa21:person[.='second']">
        <xsl:attribute name="fa:person">2</xsl:attribute>
    </xsl:template>
    <xsl:template match="@fa21:person[.='third']">
        <xsl:attribute name="fa:person">3</xsl:attribute>
    </xsl:template>

    <xsl:template match="@fa21:valency[.='intran']">
        <xsl:attribute name="fa:valency">1</xsl:attribute>
    </xsl:template>
    <xsl:template match="@fa21:valency[.='mtran']">
        <xsl:attribute name="fa:valency">2</xsl:attribute>
    </xsl:template>
    <xsl:template match="@fa21:valency[.='ditran']">
        <xsl:attribute name="fa:valency">3</xsl:attribute>
    </xsl:template>

    <xsl:template match="@fa21:predicate[.='pred']">
        <xsl:attribute name="fa:valency">yes</xsl:attribute>
    </xsl:template>

    <xsl:template match="@fa21:diminutive[.='dim']">
        <xsl:attribute name="fa:degree">dim</xsl:attribute>
    </xsl:template>

    <xsl:template match="@fa21:*" priority="-1">
        <xsl:attribute name="fa:{local-name(.)}"><xsl:value-of select="."/></xsl:attribute>
    </xsl:template>


    <xsl:template priority="-2" match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>