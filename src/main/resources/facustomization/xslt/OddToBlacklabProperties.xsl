<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  TeiLinguisticsFa
  %%
  Copyright (C) 2018 Fryske Akademy
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  -->


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="1.0">

    <xsl:output method="text"/>

    <xsl:variable name="apos">'</xsl:variable>

    <xsl:template match="/">

        <xsl:value-of select="'# GENERATED (xsltproc OddToBlacklabProperties.xsl ../corpora_linguistics.odd > fa-tei.blf.yml), ADAPT AS NEEDED'"/>
        <xsl:text><![CDATA[
namespaces:

  tei: "http://www.tei-c.org/ns/1.0"
  fa: "http://frisian.eu/tei-ud-linguistics"

fileTypeOptions:
  processing: saxon

documentPath: /tei:TEI

annotatedFields:

  contents:
            
    containerPath: tei:text

# tei:m is used for clitic and prodrop, TODO: display in Blacklab

    wordPath: .//tei:w|.//tei:pc|.//tei:m

    # capture word ids so we can refer to them from the standoff annotations
    tokenIdPath: "@xml:id"

    punctPath: ".//text()[.!='' and preceding-sibling::tei:w]|.//tei:pc |.//tei:lb"

    annotations:

    - name: word
      description: word forms
      valuePath: "string-join(.//text(),'')"
      sensitivity: si

    - name: lemma
      description: lemma of word forms
      valuePath: "if (@lemma) then @lemma else tei:m[@type='root']/@lemma"
      sensitivity: si

    - name: compound
      description: lemma of split forms
      valuePath: "let $xid := @xml:id, $l := if ($xid) then ../tei:linkGrp/tei:link[@lemma and matches(@target,'^#'||$xid||' ')]/@lemma else () return if (count($l)>1) then string-join(('ERR: ',$l),', ') else $l"
      sensitivity: si

        ]]>
</xsl:text>


      <xsl:for-each select="//tei:classSpec[@ident='att.features']//tei:attDef">
            <xsl:value-of select="'    - name: '"/>
            <xsl:value-of select="@ident"/>
        <xsl:text>
</xsl:text>
            <xsl:value-of select="'      valuePath: &quot;head(@fa:'"/>
          <xsl:value-of select="@ident"/>
          <xsl:value-of select="concat(' | tei:m[@type=',$apos,'root',$apos,']/@fa:')"/>
          <xsl:value-of select="@ident"/>
            <xsl:value-of select="')&quot;'"/>
            <xsl:text>
</xsl:text>
            <xsl:if test="tei:desc">
                <xsl:value-of select="'      description: '"/>
                <xsl:value-of select="'&quot;'"/>
                <xsl:value-of select="normalize-space(tei:desc/text())"/>
                <xsl:value-of select="'&quot;'"/>
                <xsl:text>
</xsl:text>                
            </xsl:if>
            <xsl:value-of select="'      uiType: select'"/>
        <xsl:text>

</xsl:text>
            
        </xsl:for-each>
      <xsl:for-each select="//tei:classSpec[@ident='att.linguistic']//tei:attDef">
        <xsl:value-of select="'    - name: '"/>
        <xsl:value-of select="@ident"/>
        <xsl:text>
</xsl:text>
        <xsl:value-of select="'      valuePath: &quot;head(@'"/>
        <xsl:value-of select="@ident"/>
          <xsl:value-of select="concat(' | tei:m[@type=',$apos,'root',$apos,']/@')"/>
          <xsl:value-of select="@ident"/>
          <xsl:value-of select="')&quot;'"/>
          <xsl:text>
</xsl:text>
        <xsl:if test="tei:desc">
            <xsl:value-of select="'      description: '"/>
            <xsl:value-of select="'&quot;'"/>
            <xsl:value-of select="normalize-space(tei:desc/text())"/>
            <xsl:value-of select="'&quot;'"/>
            <xsl:text>
</xsl:text>
        </xsl:if>
        <xsl:value-of select="'      uiType: select'"/>
        <xsl:text>

</xsl:text>
        
      </xsl:for-each>
      <xsl:text><![CDATA[
    inlineTags:
    # Sentence tags
    - path: .//tei:s

    standoffAnnotations:
    - path: ".//tei:linkGrp[@targFunc='head argument']/tei:link"
      type: relation
      valuePath: "./@ana"
      # Note that we make sure the root relation is indexed without a source,
      # which is required in BlackLab.
      sourcePath: "if (./@ana = 'root') then '' else replace(./@target, '^#(.+) .+$', '$1')"
      targetPath: "replace(./@target, '^.+ #(.+)$', '$1')"

metadata:

  containerPath: tei:teiHeader

  fields:

  - name: title
    valuePath: "tei:fileDesc/tei:titleStmt/tei:title"

  - name: author
    valuePath: "concat(.//tei:persName[1]/tei:forename, ' ',.//tei:persName[1]/tei:surname, normalize-space(.//tei:persName[1]/text()), ' (db key=', .//tei:persName[1]/tei:ref/@target,')')"

  - name: authorplace
    valuePath: ".//tei:person[1]/tei:residence/tei:placeName"
    displayName: Author residence

  - name: authorcode
    valuePath: ".//tei:person[1]/tei:idno"
    displayName: Author code

  - name: year
    valuePath: ".//tei:date[parent::tei:bibl or parent::tei:publicationStmt][1]/text()"
    uiType: range

  - name: source
    valuePath: ".//tei:sourceDesc/tei:msDesc/tei:head/text()"

  - name: remarks
    valuePath: ".//tei:notesStmt/tei:note/text()"

  - name: state
    valuePath: "concat('tokenized: ', boolean(//tei:w[1]), ', lemmatized ', boolean(//tei:w[@lemma][1]), ', pos tagged: ', boolean(//tei:w[@pos][1]) )"
    displayName: Document state

  - name: "language_variant"
    valuePath: "tei:profileDesc/tei:langUsage/tei:language/text()"
    displayName: Language Variant

# remove this for internal use where all material may be viewed, then generate new index!

  - name: contentViewable
    valuePath: .//tei:availability[1]/@status='free'
    displayName: free material
    
corpusConfig:

  displayName: Frisian corpora

  description: annotated frisian texts, detail level of annotation varies, midfrysk is most complete and detailed

# optIn, set this to true for internal use

  contentViewable: false

  specialFields:
    dateField: year

  annotationGroups:
    contents:
    - name: Generic aspects
      annotations:
      - word
      - lemma
      - compound
      - pos
    - name: Verb linguistics
      annotations:
      - tense
      - person
      - verbform
      - prodrop
      - clitic
      - mood
      - aux
      - voice
      - valency
    - name: Noun linguistics
      annotations:
      - case
      - number
      - gender
      - degree
      - diminutive
    - name: Other linguistics
      addRemainingAnnotations: true

  metadataFieldGroups:
    - name: Main filters
      fields:
      - year
      - author
      - language_variant
      - state
    - name: Other filters
      addRemainingFields: true

]]>
</xsl:text>
    </xsl:template>

</xsl:stylesheet>
