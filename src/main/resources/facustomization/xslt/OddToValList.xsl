<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  TeiLinguisticsFa
  %%
  Copyright (C) 2018 Fryske Akademy
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">

    <xsl:output indent="yes" method="xml" />

    <xsl:template match="/">
        <valList type="closed" xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:comment><![CDATA[
    GENERATED FILE!!!: USE FOR EXAMPLE "xsltproc OddToValList.xsl ../corpora_linguistics.odd > ValList.xml"
    ]]></xsl:comment>
            <xsl:for-each select="//tei:classSpec//tei:valItem">
                <valItem ident="{../../@ident}.{@ident}">
                    <xsl:choose>
                        <xsl:when test="tei:desc">
                            <desc>
                                <xsl:apply-templates select="../../tei:desc/tei:ref"/>
                                <xsl:value-of select="normalize-space(tei:desc/text())"/>
                            </desc>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="../../tei:desc"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </valItem>
            </xsl:for-each>
        </valList>
    </xsl:template>

    <xsl:template match="tei:ref"><xsl:copy-of select="."/><xsl:value-of select="' '"/></xsl:template>

    <xsl:template match="tei:desc">
        <xsl:copy><xsl:apply-templates/></xsl:copy>
    </xsl:template>

    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>

</xsl:stylesheet>
