<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  TeiLinguisticsFa
  %%
  Copyright (C) 2018 Fryske Akademy
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">
    
    <xsl:output indent="no" method="text" />

    <xsl:template match="/">

<![CDATA[
    // GENERATED FILE!!!: USE FOR EXAMPLE "xsltproc OddToDocJs.xsl ../corpora_linguistics.odd > LinguisticDocs.js"
]]>
const FALINGUISTICS = {

    /** @param ling use category.value (i.e. pos.noun)
    */
    getDocumentation: function(ling) {
        switch(ling.toLowerCase()) {
<xsl:for-each select="//tei:valItem[ancestor::tei:classSpec]">
            case "<xsl:value-of select="../../@ident"/>.<xsl:value-of select="@ident"/>":
                return "<xsl:value-of select="normalize-space(tei:desc/text())"/>";
</xsl:for-each>
<xsl:for-each select="//tei:valItem[ancestor::tei:elementSpec[@ident='link']]">
            case "UD-SYN.<xsl:value-of select="@ident"/>":
                return "<xsl:value-of select="normalize-space(tei:desc/text())"/>";
</xsl:for-each>
            default:
                return "undocumented";
        }
    },
    
    /** @param ling use category (i.e. pos)
    */
    getCategoryDocumentation: function(ling) {
        switch(ling.toLowerCase()) {
<xsl:for-each select="//tei:attDef[ancestor::tei:classSpec]">
            case "<xsl:value-of select="@ident"/>":
                return "<xsl:value-of select="normalize-space(tei:desc/text())"/>";
</xsl:for-each>
<xsl:for-each select="//tei:valItem[ancestor::tei:elementSpec[@ident='linkGrp']]">
            case "<xsl:value-of select="@ident"/>":
                return "<xsl:value-of select="normalize-space(tei:desc/text())"/>";
</xsl:for-each>
            default:
                return "undocumented";
        }
    }
}            
    </xsl:template>

</xsl:stylesheet>
