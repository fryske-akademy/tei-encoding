<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  TeiLinguisticsFa
  %%
  Copyright (C) 2018 Fryske Akademy
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  -->


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">
    
    <xsl:output method="text"/>

    <xsl:template match="/">

        <xsl:value-of select="'-- GENERATED (xsltproc OddToSql.xsl ../customization/v2.0/corpora_linguistics.odd > linguistics.sql), ADAPT AS NEEDED'"/>
        <xsl:text>

</xsl:text>
        <xsl:value-of select="'create table linguistic_category (id INT(10) NOT NULL auto_increment, version INT(10) NOT NULL, category varchar(16) not null, description varchar(256) not null,'"/>
            <xsl:text>
</xsl:text>
        <xsl:value-of select="'    primary key(id),'"/>
            <xsl:text>
</xsl:text>
        <xsl:value-of select="'    unique index category_unique (category));'"/>
            <xsl:text>
</xsl:text>
        <xsl:value-of select="'create table linguistic_value (id INT(10) NOT NULL auto_increment, version INT(10) NOT NULL, category_id INT(10) NOT NULL, value varchar(16) not null, description varchar(256) not null,'"/>
            <xsl:text>
</xsl:text>
        <xsl:value-of select="'    primary key(id),'"/>
            <xsl:text>
</xsl:text>
        <xsl:value-of select="'    unique index value_category_unique (category_id, value),'"/>
            <xsl:text>
</xsl:text>
        <xsl:value-of select="'    constraint group_link foreign key (category_id) references linguistic_category (id));'"/>
            <xsl:text>
</xsl:text>


        <xsl:for-each select="//tei:classSpec//tei:attDef">
            <xsl:variable name="group" select="count(preceding::tei:attDef)"/>
            <xsl:value-of select="'insert into linguistic_category (id, version, category, description) values('"/>
            <xsl:value-of select="$group"/>
            <xsl:value-of select='",0, &apos;"'/>
            <xsl:value-of select="@ident"/>
            <xsl:value-of select='"&apos;, &apos;"'/>
            <xsl:if test='contains(tei:desc,"&apos;")'>
                <xsl:message terminate="true">trouble</xsl:message>
            </xsl:if>
            <xsl:value-of select="normalize-space(tei:desc/text())"/>
            <xsl:value-of select='"&apos;);"'/>
            <xsl:text>
</xsl:text>
            <xsl:for-each select=".//tei:valItem">
                    <xsl:value-of select="'  insert into linguistic_value (id, version, category_id, value, description) values('"/>
                    <xsl:value-of select="count(preceding::tei:valItem)"/>
                    <xsl:value-of select='",0, "'/>
                    <xsl:value-of select="$group"/>
                    <xsl:value-of select='", &apos;"'/>
                    <xsl:value-of select="@ident"/>
                    <xsl:value-of select='"&apos;, &apos;"'/>
                    <xsl:if test='contains(tei:desc,"&apos;")'>
                        <xsl:message terminate="true">trouble</xsl:message>
                    </xsl:if>
                    <xsl:value-of select="normalize-space(tei:desc/text())"/>
                    <xsl:value-of select='"&apos;);"'/>
                    <xsl:text>
</xsl:text>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
