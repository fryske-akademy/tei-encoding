<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  TeiLinguisticsFa
  %%
  Copyright (C) 2018 Fryske Akademy
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  -->


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">
	<xsl:output indent="yes"/>

    <xsl:template match="/">
        <jxb:bindings xmlns:jxb="https://jakarta.ee/xml/ns/jaxb"
                       xmlns:tei="http://www.tei-c.org/ns/1.0"
                       xmlns:xjc="http://java.sun.com/xml/ns/jaxb/xjc"
                       xmlns:xs="http://www.w3.org/2001/XMLSchema"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       version="3.0"
                       xsi:schemaLocation="https://jakarta.ee/xml/ns/jaxb https://jakarta.ee/xml/ns/jaxb/bindingschema_3_0.xsd">
            
            <xsl:comment><![CDATA[
                GENERATED FILE!!!: USE FOR EXAMPLE "xsltproc OddToBind.xsl ../corpora_linguistics.odd > bind.xml"
            ]]></xsl:comment>

            <jxb:bindings schemaLocation="../xsd/corpora_linguistics.xsd" node="/xs:schema">

                <jxb:bindings node="//xs:complexContent/xs:extension/xs:attribute[@name = 'space']">
                    <jxb:property name="distinct_space"/>
                </jxb:bindings>

                <jxb:bindings node="/xs:schema/xs:attributeGroup/xs:attribute[@name = 'rendition']">
                    <jxb:property name="rendering"/>
                </jxb:bindings>

                <jxb:bindings
                        node="/xs:schema/xs:element[@name = 'recordHist']/xs:complexType/xs:choice/xs:sequence/xs:element[@ref = 'tei:change']">
                    <jxb:property name="recordHist_change"/>
                </jxb:bindings>

                <jxb:bindings
                        node="/xs:schema/xs:element[@name = 'revisionDesc']/xs:complexType/xs:choice/xs:element[@ref = 'tei:change']">
                    <jxb:property name="revisionDesc_change"/>
                </jxb:bindings>

                <xsl:for-each select="//tei:classSpec[@ident='att.linguistic']//tei:attDef">
                    <jxb:bindings
                        node="/xs:schema/xs:attributeGroup/xs:attribute[@name='{@ident}']/xs:simpleType">
                        <jxb:typesafeEnumClass name="{@ident}">
                            <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                            <xsl:for-each select=".//tei:valItem">
                                <jxb:typesafeEnumMember value="{@ident}" name="{@ident}">
                                    <xsl:attribute name="name">
                                        <xsl:choose>
                                            <xsl:when test="number(@ident) = number(@ident)">
                                                <xsl:value-of select="concat('_',@ident)"/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:value-of select="@ident"/></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>
                                    <xsl:if test="tei:desc">
                                        <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                                    </xsl:if>
                                </jxb:typesafeEnumMember>
                            </xsl:for-each>
                        </jxb:typesafeEnumClass>
                    </jxb:bindings>
                </xsl:for-each>
            </jxb:bindings>
            
            <jxb:bindings schemaLocation="../xsd/t.xsd" node="/xs:schema">
                <xsl:for-each select="//tei:classSpec[@ident='att.features']//tei:attDef">
                    <jxb:bindings
                            node="/xs:schema/xs:attribute[@name='{@ident}']/xs:simpleType">
                        <jxb:typesafeEnumClass name="{@ident}">
                            <xsl:if test="tei:desc">
                                <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                            </xsl:if>
                            <xsl:for-each select=".//tei:valItem">
                                <jxb:typesafeEnumMember value="{@ident}">
                                    <xsl:attribute name="name">
                                        <xsl:choose>
                                            <xsl:when test="number(@ident) = number(@ident) or @ident='int'">
                                                <xsl:value-of select="concat('_',@ident)"/>
                                            </xsl:when>
                                            <xsl:otherwise><xsl:value-of select="@ident"/></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:attribute>
                                    <xsl:if test="tei:desc">
                                        <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                                    </xsl:if>
                                </jxb:typesafeEnumMember>
                            </xsl:for-each>
                        </jxb:typesafeEnumClass>
                    </jxb:bindings>
                </xsl:for-each>
            </jxb:bindings>

            <jxb:bindings schemaLocation="../xsd/corpora_linguistics.xsd" node="/xs:schema">
                <xsl:for-each select="//tei:elementSpec[@ident='link']//tei:attDef[@ident='ana']">
                    <jxb:bindings
                            node="/xs:schema/xs:element[@name='link']/xs:complexType/xs:attribute[@name='{@ident}']/xs:simpleType">
                        <jxb:typesafeEnumClass name="{@ident}">
                            <xsl:if test="tei:desc">
                                <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                            </xsl:if>
                            <xsl:for-each select=".//tei:valItem">
                                <jxb:typesafeEnumMember value="{@ident}" name="ud_syn_{translate(@ident,':','_')}">
                                    <xsl:if test="tei:desc">
                                        <jxb:javadoc><xsl:value-of select="tei:desc"/></jxb:javadoc>
                                    </xsl:if>
                                </jxb:typesafeEnumMember>
                            </xsl:for-each>
                        </jxb:typesafeEnumClass>
                    </jxb:bindings>
                </xsl:for-each>
            </jxb:bindings>            
            <jxb:globalBindings>
                <xjc:simple/>
            </jxb:globalBindings>

        </jxb:bindings>
    </xsl:template>

</xsl:stylesheet>
