<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  TeiLinguisticsFa
  %%
  Copyright (C) 2018 Fryske Akademy
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">
    
    <xsl:output indent="no" method="text" />

    <xsl:template match="/">
        "grammar annotations, NOTE that \"_\" is used because \".\" isn't allowed in name, it should be replaced by \".\" during processing"
        enum GramType {
        <xsl:for-each select="//tei:classSpec//tei:valItem">
            <xsl:text>"</xsl:text>
            <xsl:value-of select="../../@ident"/>.<xsl:value-of select="@ident"/>
            <xsl:if test="tei:desc/text()">
                <xsl:text>: </xsl:text>
                <xsl:value-of select="normalize-space(tei:desc/text())"/>
            </xsl:if>
            <xsl:text>" </xsl:text>
            <xsl:value-of select="../../@ident"/>_<xsl:value-of select="@ident"/>
            <xsl:text> </xsl:text>
        </xsl:for-each>
        }
    </xsl:template>

</xsl:stylesheet>
