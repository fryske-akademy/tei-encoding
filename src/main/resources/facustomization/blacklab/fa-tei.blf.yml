# GENERATED (xsltproc OddToBlacklabProperties.xsl ../corpora_linguistics.odd > fa-tei.blf.yml), ADAPT AS NEEDED
namespaces:

  tei: "http://www.tei-c.org/ns/1.0"
  fa: "http://frisian.eu/tei-ud-linguistics"

fileTypeOptions:
  processing: saxon

documentPath: /tei:TEI

annotatedFields:

  contents:
            
    containerPath: tei:text

# tei:m is used for clitic and prodrop, TODO: display in Blacklab

    wordPath: .//tei:w|.//tei:pc|.//tei:m

    # capture word ids so we can refer to them from the standoff annotations
    tokenIdPath: "@xml:id"

    punctPath: ".//text()[.!='' and preceding-sibling::tei:w]|.//tei:pc |.//tei:lb"

    annotations:

    - name: word
      description: word forms
      valuePath: "string-join(.//text(),'')"
      sensitivity: si

    - name: lemma
      description: lemma of word forms
      valuePath: "if (@lemma) then @lemma else tei:m[@type='root']/@lemma"
      sensitivity: si

    - name: compound
      description: lemma of split forms
      valuePath: "let $xid := @xml:id, $l := if ($xid) then ../tei:linkGrp/tei:link[@lemma and matches(@target,'^#'||$xid||' ')]/@lemma else () return if (count($l)>1) then string-join(('ERR: ',$l),', ') else $l"
      sensitivity: si

        
    - name: islemma
      valuePath: "head(@fa:islemma | tei:m[@type='root']/@fa:islemma)"
      description: "Boolean, is this a base form"
      uiType: select

    - name: abbr
      valuePath: "head(@fa:abbr | tei:m[@type='root']/@fa:abbr)"
      description: "Boolean feature. Is this an abbreviation?"
      uiType: select

    - name: poss
      valuePath: "head(@fa:poss | tei:m[@type='root']/@fa:poss)"
      description: "Boolean feature. Is this word possessive?"
      uiType: select

    - name: reflex
      valuePath: "head(@fa:reflex | tei:m[@type='root']/@fa:reflex)"
      description: "Boolean feature, typically of pronouns or determiners. It tells whether the word is reflexive, i.e. refers to the subject of its clause.?"
      uiType: select

    - name: prefix
      valuePath: "head(@fa:prefix | tei:m[@type='root']/@fa:prefix)"
      description: "Boolean feature, Is this a prefix word in a compound, that usually cannot stand on its own?"
      uiType: select

    - name: prontype
      valuePath: "head(@fa:prontype | tei:m[@type='root']/@fa:prontype)"
      description: "This feature typically applies to pronouns, pronominal adjectives (determiners), pronominal numerals (quantifiers) and pronominal adverbs."
      uiType: select

    - name: case
      valuePath: "head(@fa:case | tei:m[@type='root']/@fa:case)"
      description: "Case is usually an inflectional feature of nouns."
      uiType: select

    - name: tense
      valuePath: "head(@fa:tense | tei:m[@type='root']/@fa:tense)"
      description: "Tense is typically a feature of verbs."
      uiType: select

    - name: voice
      valuePath: "head(@fa:voice | tei:m[@type='root']/@fa:voice)"
      description: "Voice is typically a feature of verbs."
      uiType: select

    - name: number
      valuePath: "head(@fa:number | tei:m[@type='root']/@fa:number)"
      description: "Number is usually an inflectional feature of nouns."
      uiType: select

    - name: person
      valuePath: "head(@fa:person | tei:m[@type='root']/@fa:person)"
      description: "Person is typically feature of personal and possessive pronouns / determiners, and of verbs."
      uiType: select

    - name: verbtype
      valuePath: "head(@fa:verbtype | tei:m[@type='root']/@fa:verbtype)"
      description: "distinctions on top of verb and aux."
      uiType: select

    - name: verbform
      valuePath: "head(@fa:verbform | tei:m[@type='root']/@fa:verbform)"
      description: "form of verb or deverbative."
      uiType: select

    - name: polite
      valuePath: "head(@fa:polite | tei:m[@type='root']/@fa:polite)"
      description: "Various languages have various means to express politeness or respect."
      uiType: select

    - name: numtype
      valuePath: "head(@fa:numtype | tei:m[@type='root']/@fa:numtype)"
      description: "numeral type."
      uiType: select

    - name: degree
      valuePath: "head(@fa:degree | tei:m[@type='root']/@fa:degree)"
      description: "Degree of comparison is typically an inflectional feature of some adjectives and adverbs."
      uiType: select

    - name: mood
      valuePath: "head(@fa:mood | tei:m[@type='root']/@fa:mood)"
      description: "Mood is a feature that expresses modality and subclassifies finite verb forms."
      uiType: select

    - name: gender
      valuePath: "head(@fa:gender | tei:m[@type='root']/@fa:gender)"
      description: "gender."
      uiType: select

    - name: hyph
      valuePath: "head(@fa:hyph | tei:m[@type='root']/@fa:hyph)"
      description: "Is this part of a hyphenated compound? Depending on tokenization, the compound may be one token or be split to several tokens; then the tokens need tags."
      uiType: select

    - name: prodrop
      valuePath: "head(@fa:prodrop | tei:m[@type='root']/@fa:prodrop)"
      description: "Added for Frisian to MISC in universaldependencies. pronoun drop, omission of pronouns because they can be inferred"
      uiType: select

    - name: clitic
      valuePath: "head(@fa:clitic | tei:m[@type='root']/@fa:clitic)"
      description: "Added for Frisian to features in universaldependencies. Most personal pronouns have a clitic form, which is the result of either vowel deletion, vowel reduction, monophthongization or schwa deletion, while there are also cases of suppletion."
      uiType: select

    - name: compound
      valuePath: "head(@fa:compound | tei:m[@type='root']/@fa:compound)"
      description: "Added for Frisian to features in universaldependencies. The univerbation of two or more words."
      uiType: select

    - name: inflection
      valuePath: "head(@fa:inflection | tei:m[@type='root']/@fa:inflection)"
      description: "Not in universaldependencies. The modification of a word to express different grammatical categories such as tense, case, voice, aspect, person."
      uiType: select

    - name: suffix
      valuePath: "head(@fa:suffix | tei:m[@type='root']/@fa:suffix)"
      description: "Not in universaldependencies Boolean feature, Is this a suffix word in a compound, that usually cannot stand on its own?"
      uiType: select

    - name: valency
      valuePath: "head(@fa:valency | tei:m[@type='root']/@fa:valency)"
      description: "Not in universaldependencies. Verb valency or valence is the number of arguments controlled by a verbal predicate."
      uiType: select

    - name: convertedfrom
      valuePath: "head(@fa:convertedfrom | tei:m[@type='root']/@fa:convertedfrom)"
      description: "Not in universaldependencies. Words belonging to one part of speech category used as another category."
      uiType: select

    - name: predicate
      valuePath: "head(@fa:predicate | tei:m[@type='root']/@fa:predicate)"
      description: "Not in universaldependencies. Predicate."
      uiType: select

    - name: construction
      valuePath: "head(@fa:construction | tei:m[@type='root']/@fa:construction)"
      description: "Not in universaldependencies. Construction."
      uiType: select

    - name: pos
      valuePath: "head(@pos | tei:m[@type='root']/@pos)"
      description: "These tags mark the core part-of-speech categories."
      uiType: select


    inlineTags:
    # Sentence tags
    - path: .//tei:s

    standoffAnnotations:
    - path: ".//tei:linkGrp[@targFunc='head argument']/tei:link"
      type: relation
      valuePath: "./@ana"
      # Note that we make sure the root relation is indexed without a source,
      # which is required in BlackLab.
      sourcePath: "if (./@ana = 'root') then '' else replace(./@target, '^#(.+) .+$', '$1')"
      targetPath: "replace(./@target, '^.+ #(.+)$', '$1')"

metadata:

  containerPath: tei:teiHeader

  fields:

  - name: title
    valuePath: "tei:fileDesc/tei:titleStmt/tei:title"

  - name: author
    valuePath: "concat(.//tei:persName[1]/tei:forename, ' ',.//tei:persName[1]/tei:surname, normalize-space(.//tei:persName[1]/text()), ' (db key=', .//tei:persName[1]/tei:ref/@target,')')"

  - name: authorplace
    valuePath: ".//tei:person[1]/tei:residence/tei:placeName"
    displayName: Author residence

  - name: authorcode
    valuePath: ".//tei:person[1]/tei:idno"
    displayName: Author code

  - name: year
    valuePath: ".//tei:date[parent::tei:bibl or parent::tei:publicationStmt][1]/text()"
    uiType: range

  - name: source
    valuePath: ".//tei:sourceDesc/tei:msDesc/tei:head/text()"

  - name: remarks
    valuePath: ".//tei:notesStmt/tei:note/text()"

  - name: state
    valuePath: "concat('tokenized: ', boolean(//tei:w[1]), ', lemmatized ', boolean(//tei:w[@lemma][1]), ', pos tagged: ', boolean(//tei:w[@pos][1]) )"
    displayName: Document state

  - name: "language_variant"
    valuePath: "tei:profileDesc/tei:langUsage/tei:language/text()"
    displayName: Language Variant

# remove this for internal use where all material may be viewed, then generate new index!

  - name: contentViewable
    valuePath: .//tei:availability[1]/@status='free'
    displayName: free material
    
corpusConfig:

  displayName: Frisian corpora

  description: annotated frisian texts, detail level of annotation varies, midfrysk is most complete and detailed

# optIn, set this to true for internal use

  contentViewable: false

  specialFields:
    dateField: year

  annotationGroups:
    contents:
    - name: Generic aspects
      annotations:
      - word
      - lemma
      - compound
      - pos
    - name: Verb linguistics
      annotations:
      - tense
      - person
      - verbform
      - prodrop
      - clitic
      - mood
      - aux
      - voice
      - valency
    - name: Noun linguistics
      annotations:
      - case
      - number
      - gender
      - degree
      - diminutive
    - name: Other linguistics
      addRemainingAnnotations: true

  metadataFieldGroups:
    - name: Main filters
      fields:
      - year
      - author
      - language_variant
      - state
    - name: Other filters
      addRemainingFields: true


