
package org.fa.tei.jaxb.v2_x;

/*-
 * #%L
 * TeiLinguisticsFa
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import jakarta.xml.bind.JAXBException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.fa.tei.xslt.XslTransformer;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

/**
 *
 * @author eduard
 */
public class TestXslt {
    
    @Test
    public void testXslt() throws IOException, SAXException, JAXBException, TransformerConfigurationException, TransformerException {
        XslTransformer transformer = new XslTransformer("src/main/resources/facustomization/xslt/OddToBlacklabProperties.xsl");
        StringWriter streamTransform = transformer.streamTransform(new BufferedReader(new FileReader("src/main/resources/facustomization/corpora_linguistics.odd")), new StringWriter(4096));
        Files.write(new File("target/test.yml").toPath(), streamTransform.toString().getBytes(), StandardOpenOption.CREATE);
    }
    
}
