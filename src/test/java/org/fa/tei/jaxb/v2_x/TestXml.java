
package org.fa.tei.jaxb.v2_x;

/*-
 * #%L
 * TeiLinguisticsFa
 * %%
 * Copyright (C) 2018 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import jakarta.xml.bind.JAXBException;
import org.fa.tei.jaxb.facustomization.TEI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;
import org.fa.tei.validation.*;
import name.dmaus.schxslt.SchematronException;
/**
 *
 * @author eduard
 */
public class TestXml {
    
    @Test
    public void testXml() throws IOException, SAXException, JAXBException, SchematronException {
        byte[] xml = Files.readAllBytes(new File("src/test/resources/gj5040.xml").toPath());
        ValidationHelper.validateXml(new String(xml));
        TEI fromXML = ValidationHelper.fromXML(new StringReader(new String(xml)), TEI.class);
        String copy = ValidationHelper.toXML(fromXML);
        RngValidationHelper.validateRngSchematron(copy);

        xml = Files.readAllBytes(new File("src/test/resources/test.xml").toPath());
        ValidationHelper.validateXml(new String(xml));
        fromXML = ValidationHelper.fromXML(new StringReader(new String(xml)), TEI.class);
        copy = ValidationHelper.toXML(fromXML);
        RngValidationHelper.validateRngSchematron(copy);

        xml = Files.readAllBytes(new File("src/test/resources/test-fout.xml").toPath());
        try {
            ValidationHelper.validateSchematron(new String(xml));
            Assertions.fail("Exception expected for @lemma on link/@ana=punct");
        } catch (SchematronException e) {
            // expected
        }
    }
    
}
